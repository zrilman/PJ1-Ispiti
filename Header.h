#pragma once
#include <iostream>
#include <algorithm>
#include <functional>

class Original
{
public:
	Original(int cap) :cap(cap)
	{
		arr = new int[cap];
		std::fill(arr, arr + cap, 5);
	}
	Original(const Original& other)
	{
		copy(other);
	}
	Original(Original&& other)
	{
		move(std::move(other));
	}
	~Original()
	{
		delete[] arr;
		arr = nullptr;
		cap = 0;
	}
	Original& operator= (const Original& other)
	{
		if (this != &other)
			copy(other);
		return *this;
	}
	Original& operator= (Original&& other)
	{
		if (this != &other)
			move(std::move(other));
		return *this;
	}
	Original* clone()
	{
		return this;
	}
private:
	int* arr;
	int cap;

	void copy(const Original& other)
	{
		delete[] arr;
		arr = new int[cap = other.cap];
		std::copy(other.arr, other.arr + cap, arr);
	}
	void move(Original&& other)
	{
		cap = std::move(other.cap);
		arr = std::move(other.arr);
		other.arr = nullptr;
		other.cap = 0;
	}
	friend std::ostream& operator<<(std::ostream& os, const Original& print)
	{
		std::for_each(print.arr, print.arr + print.cap, [&os](const int& a) { os << a << ' '; });
		return os << std::endl;
	}
};

class Derived1 : public Original
{
public:
	Derived1(int n) :Original(n) { }
	Derived1* clone()
	{
		return this;
	}
};

class Derived2 :public Original
{
public:
	Derived2(int n) : Original(n) {}
	Derived2* clone()
	{
		return this;
	}
};