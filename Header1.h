#include <iostream>
#include <tuple>
#include <vector>
#include <functional>
#include <type_traits>
#include <algorithm>
#include <iomanip>

namespace Kita
{
	template<class T>
	using majtapl = std::tuple<int, T>;
	template<class T>
	using majFanksn = std::function<bool(T)>;

	template<class T, int N = 0>
	class tapl
	{
		static_assert(std::is_copy_constructible<T>::value, "Kita baki");

		T* arr;
		int size;
		std::vector <majtapl<T>> vec;

		void move(tapl&&);
		const majFanksn<T>& f;
		template<class T, int N>
		friend std::ostream& operator<< (std::ostream&, const tapl<T, N>&);
	public:
		tapl(const majFanksn<T>&) noexcept;
		tapl(tapl&&) noexcept;
		~tapl() noexcept;
		tapl(const tapl&) = delete;
		tapl& operator=(const tapl&) = delete;
		tapl& operator=(tapl&&);
		tapl<T, N>& add(const std::initializer_list<T>&) noexcept(false);
		bool exists(const T&);
		majtapl<T>& find(const T&) const;
		void remove(const int&);
		tapl<T, N>& reverse();
		T findMin();
		T findMax();
	};

	template<class T, int N>
	tapl<T, N>::tapl(const majFanksn<T>& ff) noexcept: f(ff), size(0)
	{
		arr = new T[N];
		std::fill(arr, arr + size, 0);
	}

	template<class T, int N>
	tapl<T, N>::tapl(tapl&& other) noexcept
	{
		move(std::move(other));
	}

	template<class T, int N>
	tapl<T, N>::~tapl() noexcept
	{
		delete[] arr;
		arr = nullptr;
		vec.erase(vec.begin(), vec.end());
	}

	template<class T, int N>
	void tapl<T, N>::move(tapl&& other)
	{
		arr = std::move(other.arr);
		size = std::move(size);
		vec = std::move(other.vec);
	}

	template<class T, int N>
	tapl<T, N>& tapl<T, N>::operator= (tapl&& other)
	{
		if (this != &other)
			move(std::move(other));
		return *this;
	}

	template<class T, int N>
	tapl<T, N>& tapl<T, N>::add(const std::initializer_list<T>& list) noexcept(false)
	{
		int i = 1;
		for (const auto& a : list)
			if (f(a) && !exists(a))
				vec.push_back({ i++,a });
			else
				arr[size++] = a;
		return *this;
	}

	template<class T, int N>
	bool tapl<T, N>::exists(const T& toFind)
	{
		return vec.end() != std::find_if(vec.begin(), vec.end(), [toFind](const auto& toCheck) { return std::get<1>(toCheck) == toFind; });
	}

	template<class T, int N>
	void tapl<T, N>::remove(const int& index)
	{
		if (index > size || index < 0)
			throw std::out_of_range("Index out of range.");

		else
			vec.erase(vec.begin() + index);
	}

	template<class T, int N>
	tapl<T, N>& tapl<T, N>::reverse()
	{
		std::reverse(vec.begin(), vec.end());
		return *this;
	}

	template<class T, int N>
	T tapl<T, N>::findMin()
	{
		return std::get<0>(*std::min_element(vec.begin(), vec.end()));
	}

	template<class T, int N>
	T tapl<T, N>::findMax()
	{
		return std::get<0>(*std::max_element(vec.begin(), vec.end()));
	}

	template<class T, int N>
	majtapl<T>& tapl<T, N>::find(const T& toFind) const
	{
		auto it = std::find_if(vec.begin(), vec.end(), [toFind](const auto& toCheck) { return std::get<1>(toCheck) == toFind; });
		return vec.end() != it ? *it : std::make_tuple(0, 0);
	}

	template<class T, int N>
	std::ostream& operator<< (std::ostream& os, const tapl <T, N>& tapl)
	{
		os << "Vector: " << std::endl;
		for (const auto& it : tapl.vec)
			os << std::setw(2) << std::get<0>(it) << ":" << std::setw(3) << std::right << std::get<1>(it) << std::endl;
		os << std::endl << "Arr: ";
		std::for_each(tapl.arr, tapl.arr + tapl.size, [&os](const T& a) { os << a << ' '; });
		return os << std::endl;
	}
}

namespace std
{
	template<class T, int N>
	struct hash<Kita::tapl<T, N>>
	{
		size_t operator()(const Kita::tapl& toHash)
		{
			std::cout << "Hesiranje.. ";
			return std::get<1>(toHash.vec.begin()) ^ std::get<1>(toHash.vec.end());
		}
	};
}