#include "Vector2.h"
#include <iostream>

Vector2::Vector2(double a, double b) noexcept
{
	v2 = std::make_tuple(a, b);
}

Vector2::Vector2(std::tuple<double, double> a) noexcept
{
	v2 = std::make_tuple(std::get<0>(a), std::get<1>(a));
}
Vector2 Vector2::operator+(const Vector2& rhs)
{
	return Vector2(std::get<0>(v2) + std::get<0>(rhs.v2), std::get<1>(v2) + std::get<1>(rhs.v2));
}

double Vector2::get0() const
{
	return std::get<0>(v2);
}

double Vector2::get1() const
{
	return std::get<1>(v2);
}

std::ostream& operator<< (std::ostream& os, const Vector2& toPrint)
{
	return os << '[' << std::get<0>(toPrint.v2) << ',' << std::get<1>(toPrint.v2) << ']';
}

VectorSet& VectorSet::operator+=(const Vector2& toAdd) noexcept(false)
{
	vec.insert(toAdd);
	return *this;
}

void VectorSet::print(std::ostream& os) const noexcept
{
	os << "Vector set: ";
	std::for_each(vec.begin(), vec.end(), [&os](const Vector2& a) { os << a << ' '; });
	os << std::endl;
}

std::ostream& operator<< (std::ostream& os, const VectorSet& vs)
{
	vs.print(os);
	return os;
}

FilteredVectorSet& FilteredVectorSet::operator+= (const Vector2& vs) noexcept(false)
{
	if (f(vs))
		VectorSet::operator+=(vs);
	else
		throw "Invalid vector propery.";
	return *this;
}