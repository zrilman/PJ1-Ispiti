#pragma once
#include <tuple>
#include <functional>
#include <algorithm>
#include <unordered_set>

class Vector2;
using myFunction = std::function<bool(Vector2)>;

class Vector2
{
public:
	Vector2(double = 0, double = 0) noexcept;
	explicit Vector2(std::tuple<double, double>) noexcept;
	Vector2 operator+ (const Vector2&);
	double get0() const;
	double get1() const;
private:
	std::tuple<double, double> v2;
	friend std::ostream& operator<< (std::ostream&, const Vector2&);
};

namespace std
{
	template<>
	struct equal_to<Vector2>
	{
		bool operator()(const Vector2& lhs, const Vector2& rhs) const
		{
			return (lhs.get0() == rhs.get0()) && (lhs.get1() == rhs.get1());
		}
	};

	template<>
	struct hash<Vector2>
	{
		size_t operator()(const Vector2& vec) const
		{
			return (size_t)vec.get0() ^ (size_t)vec.get1();
		}
	};
}

class VectorSet
{
public:
	VectorSet() = default;
	virtual VectorSet& operator+=(const Vector2&) noexcept(false);
	void print(std::ostream&) const noexcept;
protected:
	std::unordered_set<Vector2> vec;
private:
	friend std::ostream& operator<< (std::ostream&, const VectorSet&);
};

class FilteredVectorSet : public VectorSet
{
public:
	FilteredVectorSet(const myFunction& ff) : f(ff) {}
	FilteredVectorSet& operator+= (const Vector2&) noexcept(false);
private:
	const myFunction& f;
};