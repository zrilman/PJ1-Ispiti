#include "Header.h"
#include "Header1.h"
#include "Vector2.h"
#include <fstream>
using namespace Kita;
using std::cout;
using std::endl;
constexpr int f(int a = 20)
{
	return a* (a == 5 ? f(a / 2) : 5);
}


void main()
{
	/*majFanksn<int> ff = [](const int& a) {return a % 2 == 0; };
	tapl<int, 100> blabla(ff);
	blabla.add({ 4,65,64,4,64,6,4,64,4,2,12,7,32,8 });
	blabla.add({ 4,65,64,4,64,6,4,64,4,2,12,7,32,8 });
	cout << blabla << endl;
	cout << "4 in vector? " << std::boolalpha << blabla.exists(4) << endl;
	cout << "-1 in vector? " << std::boolalpha << blabla.exists(-1) << endl;
	cout << std::get<0>(blabla.find(64)) << ' ' << std::get<1>(blabla.find(64)) << endl;
	cout << blabla.reverse() << endl;
	cout << "Max: " << blabla.findMax() << endl;
	cout << "Min: " << blabla.findMin() << endl;
	try
	{
		blabla.remove(-5);
		blabla.remove(2);

	}
	catch (std::exception& ex)
	{
		cout << ex.what() << endl;
	}
	cout << blabla << endl;
	Original org(5);
	cout << org << endl;
	Derived1 d1(6);
	cout << d1;
	Derived2 d2(8);
	cout << d2 << endl;*/

	Vector2 v1;
	Vector2 v2(10, 10);
	Vector2 v3 = v2 + v1;
	std::tuple<double, double> tap(-10, 10);
	Vector2 v4(tap);

	VectorSet vecSet;
	(((vecSet += v1) += v2) += v3) += v4; cout << vecSet;

	const myFunction& f = [](Vector2 v) { return  v.get0() >= 0 && v.get1() >= 0; };
	FilteredVectorSet filteredVecSet(f);
	try
	{
		(((filteredVecSet += v1) += v2) += v3) += v4;
	}
	catch (char* ex)
	{
		cout << ex << endl;
	}
	std::ofstream ofs("output.txt");
	if (ofs.is_open())
	{
		ofs << filteredVecSet;
		ofs.close();
	}
	std::cin.ignore();
}
